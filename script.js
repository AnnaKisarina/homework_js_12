const btn = document.querySelectorAll('.btn');
window.addEventListener('keydown', flash);

function flash (e) {
    let arr = [];

    for (let key in btn) {
        if (!isNaN(key)) {
            arr[btn[key].textContent.toLowerCase()] = key;
        }
    }

    let el = null;
    switch(e.keyCode) {
        case 13:
            el= btn[arr['enter']];
            break;
        case 83:
            el = btn[arr['s']];
            break;
        case 69:
           el = btn[arr['e']];
            break;
        case 79:
            el = btn[arr['o']];
            break;
        case 78:
            el = btn[arr['n']];
            break;
        case 76:
            el = btn[arr['l']];
            break;
        case 90:
            el = btn[arr['z']];
            break;
    }

    let q = document.body.querySelector('.active');

    if (q && el) {
        q.classList.remove('active');
    }
    
    el && el.classList.add('active');
}
